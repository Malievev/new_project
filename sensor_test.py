#!/usr/bin/env python

from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
from std_msgs.msg import ColorRGBA
import rospy
import tf
import math
import random
import numpy as np
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from location.msg import Cone
from location.msg import Perception



MARKERS_MAX = 1000099
marker_array = MarkerArray()
count = 0
class SensorTest(object):

    def __init__(self):

        # 1.2 Initialize ROS node
        #rospy.init_node('SensorSimulate')

        # define state variables
        self.position = None
        self.carpostion_x = 200
        self.carpostion_y = 235
        self.v = None
        self.yaw = None
        self.car_angle=None
        self.cone_list=[]
        self.showedconeslist=[]

        rospy.init_node('sensor_sender')
        rospy.Subscriber('/detection', Perception, self.callback)
        #rospy.Subscriber('/carla/ego_vehicle/odometry', Odometry, self.callbackfunktion)
        rospy.loginfo('lol')
        topic = 'visualization_marker'
        topicforcar = 'visualization_cartopic'
        self.publishercar = rospy.Publisher('visualization_cartopic', Marker, queue_size=10)
        self.publisher = rospy.Publisher(topic, MarkerArray, queue_size=10)



        #rospy.spin()
        #self.loop()
        #self.showcarposition()
        self.schowconeposition()

    def callback(self, data):
        #rospy.loginfo("I")
        #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data)
        cone=data.Cone_detections[0]#[0].color
        #rospy.loginfo(cone.color)
        self.cone_list += [cone]
        #rospy.loginfo(self.cone_list)
        #rospy.loginfo("I")


    def callbackfunktion(self, position):
        self.position = position.pose
        #rospy.loginfo("I")
        self.carpostion_x = self.position.pose.position.x
        self.carpostion_y = self.position.pose.position.y
        #rospy.loginfo('Carposition: x: {}, y: {}'.format(self.carpostion_x, self.carpostion_y))
        #self.yaw = self.get_yaw_last_position()
        #rad_to_deg = lambda x: 180.0/math.pi * x * -1
        #self.car_angle=rad_to_deg(self.yaw)
        #rospy.loginfo('angle: {}'.format(self.car_angle))
        rospy.sleep(10)


    # method to convert quaternions to yaw angle
    def get_yaw_last_position(self):
        quaternion = (
                        self.position.pose.orientation.x,
                        self.position.pose.orientation.y,
                        self.position.pose.orientation.z,
                        self.position.pose.orientation.w)

        euler = tf.transformations.euler_from_quaternion(quaternion)
        yaw = euler[2]

        return yaw
    #def loop(self):
    #    while not rospy.is_shutdown():
    #        for cone in (self.cone_list)
    #            self.showcarposition(cone)
    #            self.schowconeposition(cone)

    def showcarposition(self):#, cone):
        rospy.loginfo("Icar")
        marker = Marker()
        marker.header.frame_id = "map"
        #marker.type = marker.POINTS
        marker.action = marker.ADD
        marker.type = marker.CYLINDER
        offset=200
        x=self.carpostion_x - offset
        y=self.carpostion_y -230
        marker.pose.position.x=x  #random.randrange(-5,5,1)
        marker.pose.position.y =y#((-1)carpostion_y)-230 #0
        marker.pose.position.z = 0 #random.randrange(-5,5,1)
        marker.pose.orientation.w = 1.0
        marker.scale.x = 0.4
        marker.scale.y = 0.4
        marker.scale.z = 0.01
        marker.color.a = 0.9
        marker.color.r = 255.0
        marker.color.g = 0.0
        marker.color.b = 0.0

        self.publishercar.publish(marker)

    def schowconeposition(self):#, cone):
        count=0
        rospy.loginfo("I")
        while not rospy.is_shutdown():
            rospy.sleep(0.1)
            #rospy.loginfo("while")
            for cone in (self.cone_list):
                #print(cone)
                if cone not in self.showedconeslist:
                    marker = Marker()
                    marker_car = Marker()
                    marker.header.frame_id = "map"
                    marker_car.header.frame_id = "map"
                    #marker.type = marker.POINTS
                    marker.action = marker.ADD
                    marker_car.action = marker.ADD
                    marker.type = marker.CYLINDER
                    marker_car.type = marker.CYLINDER

                    #marker.pose.position.x= random.randrange(-5,5,1)
                    #marker.pose.position.y = 0
                    #marker.pose.position.z = random.randrange(-5,5,1)
                    #rad_to_deg = lambda x: 180.0/math.pi * x
                    #x=self.carpostion_x
                    #y=self.carpostion_y
                    #a=math.atan2(y, x)
                    #aa=rad_to_deg(a)
                    angle=cone.angle - cone.heading#(aa) - 1.57
                    print('angle',cone.angle)
                    print('heading', cone.heading)
                    x=cone.xpos + (cone.distance*math.cos(angle))# (d * cos(alpha))
                    y=cone.ypos + (cone.distance*math.sin(angle))
                    rospy.loginfo(cone.xpos)
                    rospy.loginfo(cone.ypos)
                    #rospy.loginfo('Carposition: x: {}, y: {}'.format(x, y))
                    marker.pose.position.x= (x)
                    print('cone x', x)
                    print('cone x', marker.pose.position.x)
                    marker_car.pose.position.x= (cone.xpos) #-200
                    print('car x', cone.xpos)
                    print('car x', marker_car.pose.position.x)

                    marker.pose.position.y = y
                    print('cones y',y)
                    print('cones y', marker.pose.position.y)
                    ypose=cone.ypos
                    marker_car.pose.position.y = ypose
                    print('car y',ypose)
                    print('car y', marker_car.pose.position.y)
                    #rospy.loginfo('Carposition: x: {}, y: {}'.format(x, y))
                    #rospy.loginfo('Carposition: x: {}, y: {}'.format(cone.xpos, ypose))
                    marker.pose.position.z =1# random.randrange(-5,5,1)
                    marker_car.pose.position.z =1
                    marker.pose.orientation.w = 1.0
                    marker_car.pose.orientation.w = 1.0
                    marker.scale.x = 1.0
                    marker_car.scale.x = 1.0
                    marker.scale.y = 1.0
                    marker_car.scale.y = 1.0
                    marker.scale.z = 0.01
                    marker_car.scale.z = 0.01
                    marker.color.a = 0.9
                    marker_car.color.a = 0.9
                    marker_car.color.r = 255.0
                    marker_car.color.g = 0.0
                    marker_car.color.b = 0.0
                    if cone.color=="y":
                        marker.color.r = 255.0
                        marker.color.g = 255.0
                        marker.color.b = 0.0
                    elif cone.color=="b":
                        marker.color.r = 0.0
                        marker.color.g = 0.0
                        marker.color.b = 255.0
                    elif cone.color=="ob":
                        marker.color.r = 255.0
                        marker.color.g = 165.0
                        marker.color.b = 0.0
                        marker.scale.x = 1.5
                        marker.scale.y = 1.5
                        marker.scale.z = 0.01
                    elif cone.color=="o":
                        marker.color.r = 255.0
                        marker.color.g = 165.0
                        marker.color.b = 0.0
                    self.showedconeslist+=[cone]
                    if(count > MARKERS_MAX):
  	                     marker_array.markers.pop(0)
                    marker_array.markers.append(marker)
                    #marker_array.markers.append(marker_car)
                    id = 0
                    for m in marker_array.markers:
                        m.id = id
                        id += 1
                             # Publish the MarkerArray
                    #print(marker_array)
                    self.publisher.publish(marker_array)
                    self.publishercar.publish(marker_car)
                    count += 1

                    rospy.sleep(0.1)




if __name__ == '__main__':
    try:
        SensorTest()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start mission planner node.')
        pass
